function submitButton(){
    var form = document.getElementById("sample-form-1");
    // form.submit();

    form.addEventListener('submit',(event)=>{
        // prevent submit
        event.preventDefault();
    });

    // get value from input user
    const name = form.elements['name'];
    let fullName = name.value;

    // show value
    window.alert(fullName);
}
function showAlert(){
    window.alert();
}

$(document).ready(function() {
    // Selector Tag
    $('h1').css('color','red');

    // Selector Class
    $('.selector-class').css('color','blue');

    // Selector Id
    $('#selector-id').css('color','green');

    // Event click
    $('#one-click').click(function(){
        alert("Event Click 1 Clicked");
    });
    $('#two-click').dblclick(function(){
        // alert("Event Click 2 Clicked");
        if($(this).css('background-color') == "rgb(255, 0, 0)") {
            $(this).css('background-color','white');
            alert("If Event Click 2 Clicked");
        } else {
            $(this).css('background-color','red');
            alert("Else Event Click 2 Clicked");
        }
    });

    // Mouse event
    $('.mouse').mouseenter(function() {
        $(this).css('color','red');
    });
    $('.mouse').mouseleave(function() {
        $(this).css('color','blue');
    });

    // Event Keydown
    $('#keydown').keydown(function() {
        $('#output-keydown').text($(this).val());
    });

    // Event Keyup
    $('#keyup').keyup(function() {
        $('#output-keyup').text($(this).val());
    });

    // Animation -----------
    // Hide
    $('#button-hide').click(function() {
        // $('#kotak').hide(); // without delay
        // $('#kotak').hide(1000); // delay: 1000 ms
        $('#kotak').hide("fast"); // delay: "fast"
    });
    // Show
    $('#button-show').click(function() {
        $('#kotak').show("fast"); // delay: "fast"
    });
    // Fade in
    $('#button-fadeIn').click(function() {
        $('#kotak').fadeIn("fast"); // delay: "fast"
    });
    // Fade out
    $('#button-fadeOut').click(function() {
        $('#kotak').fadeOut("fast"); // delay: "fast"
    });
    // Fade toggle
    $('#button-fadeToggle').click(function() {
        $('#kotak').fadeToggle("fast"); // delay: "fast"
    });
    // Fade to
    $('#button-fadeTo').click(function() {
        $('#kotak').fadeTo("fast",0.4); // delay: "fast", oppacity: 0.4
    });

    // Slide Up
    $('#button-slideUp').click(function() {
        $('#kotak').slideUp("slow");
    });
    // Slide Down
    $('#button-slideDown').click(function() {
        $('#kotak').slideDown("slow");
    });
    // Slide Toggle
    $('#button-slideToggle').click(function() {
        $('#kotak').slideToggle("slow");
    });

    // Animate
    $('#button-moveLeft').click(function() {
        $('#kotak').animate({left:'-=50px'},"fast");
    });
    $('#button-moveRight').click(function() {
        $('#kotak').animate({left:'+=50px'},"fast");
    });

    // clear queue
    $('#button-moveStart').click(function() {
        $('#kotak').animate({left:'+=500px'},4000);
        $('#kotak').queue(function(){});
    });
    $('#button-moveStop').click(function() {
        $('#kotak').clearQueue();
        $('#kotak').stop();
    });

    // Delay
    $('#button-delay').click(function() {
        $('#kotak').animate({left:'+=50px'},2000).delay(500).animate({left:'-=50px'},2000);
    });
})